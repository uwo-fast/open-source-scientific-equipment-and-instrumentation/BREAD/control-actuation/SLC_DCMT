EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR06
U 1 1 5FA66343
P 1800 4600
F 0 "#PWR06" H 1800 4350 50  0001 C CNN
F 1 "GND" H 2150 4650 50  0000 C CNN
F 2 "" H 1800 4600 50  0001 C CNN
F 3 "" H 1800 4600 50  0001 C CNN
	1    1800 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 4500 1750 4550
Wire Wire Line
	1750 4550 1800 4550
Wire Wire Line
	1850 4550 1850 4500
Wire Wire Line
	1800 4550 1800 4600
Connection ~ 1800 4550
Wire Wire Line
	1800 4550 1850 4550
$Comp
L power:+5V #PWR07
U 1 1 5FA67628
P 1950 2400
F 0 "#PWR07" H 1950 2250 50  0001 C CNN
F 1 "+5V" H 1965 2573 50  0000 C CNN
F 2 "" H 1950 2400 50  0001 C CNN
F 3 "" H 1950 2400 50  0001 C CNN
	1    1950 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 2400 1950 2500
$Comp
L power:+12V #PWR05
U 1 1 5FA6990A
P 1650 2400
F 0 "#PWR05" H 1650 2250 50  0001 C CNN
F 1 "+12V" H 1665 2573 50  0000 C CNN
F 2 "" H 1650 2400 50  0001 C CNN
F 3 "" H 1650 2400 50  0001 C CNN
	1    1650 2400
	1    0    0    -1  
$EndComp
NoConn ~ 1250 2900
NoConn ~ 1250 3000
Wire Wire Line
	800  3300 1250 3300
Text Label 800  3200 0    50   ~ 0
INT
Text Label 2600 4000 2    50   ~ 0
I2C_CLK
Text Label 2600 3900 2    50   ~ 0
I2C_DAT
Wire Wire Line
	2600 3900 2250 3900
Wire Wire Line
	2250 4000 2600 4000
NoConn ~ 1850 2500
NoConn ~ 2250 2900
NoConn ~ 2250 3000
NoConn ~ 2250 3300
Text Label 800  3100 0    50   ~ 0
E_STOP
Wire Wire Line
	800  3200 1250 3200
Wire Wire Line
	1250 3100 800  3100
$Comp
L OCI_UPL_2_Capacitors:10uF_t C1
U 1 1 5FA94020
P 1600 1350
F 0 "C1" H 1715 1396 50  0000 L CNN
F 1 "10uF_t" H 1715 1305 50  0000 L CNN
F 2 "OCI_UPL_FOOTPRINTS:C_2312" H 1600 1100 30  0001 C CNN
F 3 "https://www.digikey.com/short/qcd8n7" H 1600 1550 30  0001 C CNN
F 4 "T491C106K025AT" H 1600 1600 30  0001 C CNN "Part #"
F 5 "2.021" H 1600 1150 30  0001 C CNN "UPL #"
	1    1600 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5FA94026
P 1600 1550
F 0 "#PWR04" H 1600 1300 50  0001 C CNN
F 1 "GND" H 1605 1377 50  0000 C CNN
F 2 "" H 1600 1550 50  0001 C CNN
F 3 "" H 1600 1550 50  0001 C CNN
	1    1600 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 1200 1600 1150
Wire Wire Line
	1600 1550 1600 1500
$Comp
L Mechanical:MountingHole H1
U 1 1 5FAB1765
P 1100 4900
F 0 "H1" H 1200 4946 50  0000 L CNN
F 1 "MountingHole" H 1200 4855 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1100 4900 50  0001 C CNN
F 3 "~" H 1100 4900 50  0001 C CNN
	1    1100 4900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5FAB1B3E
P 1850 4900
F 0 "H3" H 1950 4946 50  0000 L CNN
F 1 "MountingHole" H 1950 4855 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1850 4900 50  0001 C CNN
F 3 "~" H 1850 4900 50  0001 C CNN
	1    1850 4900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5FAB217D
P 1100 5100
F 0 "H2" H 1200 5146 50  0000 L CNN
F 1 "MountingHole" H 1200 5055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1100 5100 50  0001 C CNN
F 3 "~" H 1100 5100 50  0001 C CNN
	1    1100 5100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5FAB25F7
P 1850 5100
F 0 "H4" H 1950 5146 50  0000 L CNN
F 1 "MountingHole" H 1950 5055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1850 5100 50  0001 C CNN
F 3 "~" H 1850 5100 50  0001 C CNN
	1    1850 5100
	1    0    0    -1  
$EndComp
Text Label 800  3300 0    50   ~ 0
SYNC
$Comp
L Connector:Conn_01x10_Female J1
U 1 1 5FE6B3C7
P 1400 1450
F 0 "J1" H 1428 1380 50  0000 L CNN
F 1 "Conn_01x10_Female" H 1428 1335 50  0001 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x10_P2.54mm_Horizontal" H 1400 1450 50  0001 C CNN
F 3 "~" H 1400 1450 50  0001 C CNN
	1    1400 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR01
U 1 1 5FE6D224
P 1100 1000
F 0 "#PWR01" H 1100 850 50  0001 C CNN
F 1 "+12V" H 1115 1173 50  0000 C CNN
F 2 "" H 1100 1000 50  0001 C CNN
F 3 "" H 1100 1000 50  0001 C CNN
	1    1100 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5FE6E4CA
P 1150 2000
F 0 "#PWR02" H 1150 1750 50  0001 C CNN
F 1 "GND" H 1155 1827 50  0000 C CNN
F 2 "" H 1150 2000 50  0001 C CNN
F 3 "" H 1150 2000 50  0001 C CNN
	1    1150 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 2000 1150 1950
Wire Wire Line
	1150 1050 1200 1050
Wire Wire Line
	1200 1950 1150 1950
Connection ~ 1150 1950
Wire Wire Line
	1100 1000 1100 1150
Wire Wire Line
	1100 1150 1200 1150
Wire Wire Line
	1200 1850 1100 1850
Wire Wire Line
	1100 1850 1100 1150
Connection ~ 1100 1150
Wire Wire Line
	1200 1450 1150 1450
Connection ~ 1150 1450
Wire Wire Line
	1150 1450 1150 1050
Text Label 750  1250 0    50   ~ 0
I2C_CLK
Text Label 750  1350 0    50   ~ 0
I2C_DAT
Text Label 750  1550 0    50   ~ 0
E_STOP
Text Label 750  1650 0    50   ~ 0
INT
Text Label 750  1750 0    50   ~ 0
SYNC
Wire Wire Line
	1200 1250 750  1250
Wire Wire Line
	750  1350 1200 1350
Wire Wire Line
	1150 1950 1150 1450
Wire Wire Line
	1200 1550 750  1550
Wire Wire Line
	750  1650 1200 1650
Wire Wire Line
	750  1750 1200 1750
$Comp
L power:+12V #PWR03
U 1 1 5FE73FEC
P 1600 1150
F 0 "#PWR03" H 1600 1000 50  0001 C CNN
F 1 "+12V" H 1615 1323 50  0000 C CNN
F 2 "" H 1600 1150 50  0001 C CNN
F 3 "" H 1600 1150 50  0001 C CNN
	1    1600 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 2400 1650 2500
$Comp
L OCI_UPL_2_Capacitors:10uF_t C2
U 1 1 5FE84189
P 2100 1350
F 0 "C2" H 2215 1396 50  0000 L CNN
F 1 "10uF_t" H 2215 1305 50  0000 L CNN
F 2 "OCI_UPL_FOOTPRINTS:C_2312" H 2100 1100 30  0001 C CNN
F 3 "https://www.digikey.com/short/qcd8n7" H 2100 1550 30  0001 C CNN
F 4 "T491C106K025AT" H 2100 1600 30  0001 C CNN "Part #"
F 5 "2.021" H 2100 1150 30  0001 C CNN "UPL #"
	1    2100 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5FE8418F
P 2100 1550
F 0 "#PWR09" H 2100 1300 50  0001 C CNN
F 1 "GND" H 2105 1377 50  0000 C CNN
F 2 "" H 2100 1550 50  0001 C CNN
F 3 "" H 2100 1550 50  0001 C CNN
	1    2100 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 1200 2100 1150
Wire Wire Line
	2100 1550 2100 1500
$Comp
L power:+5V #PWR08
U 1 1 5FE8466D
P 2100 1150
F 0 "#PWR08" H 2100 1000 50  0001 C CNN
F 1 "+5V" H 2115 1323 50  0000 C CNN
F 2 "" H 2100 1150 50  0001 C CNN
F 3 "" H 2100 1150 50  0001 C CNN
	1    2100 1150
	1    0    0    -1  
$EndComp
Wire Notes Line
	700  5250 2650 5250
Wire Notes Line
	2650 5850 2650 1350
Wire Notes Line
	2650 750  700  750 
Wire Notes Line
	700  750  700  5250
Wire Notes Line
	650  5300 2700 5300
Wire Notes Line
	2700 5300 2700 600 
Wire Notes Line
	2700 600  650  600 
Wire Notes Line
	650  600  650  5300
Text Notes 750  750  0    100  ~ 0
Core Slice Components
$Comp
L Graphic:Logo_Open_Hardware_Small Logo1
U 1 1 5FE4A934
P 10950 6850
F 0 "Logo1" H 10950 7125 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 10950 6625 50  0001 C CNN
F 2 "Symbol:OSHW-Symbol_6.7x6mm_SilkScreen" H 10950 6850 50  0001 C CNN
F 3 "~" H 10950 6850 50  0001 C CNN
	1    10950 6850
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:LMD18200 U1
U 1 1 609DCCF9
P 5750 2100
F 0 "U1" H 4900 2800 50  0000 C CNN
F 1 "LMD18200" H 4900 2700 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-11_P3.4x5.08mm_StaggerOdd_Lead4.85mm_Vertical" H 4300 750 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmd18200.pdf" H 5650 2100 50  0001 C CNN
	1    5750 2100
	1    0    0    -1  
$EndComp
Text Label 4800 1800 2    50   ~ 0
MC1
Text Label 4800 2100 2    50   ~ 0
DIR1
Text Label 4800 2400 2    50   ~ 0
BR1
Wire Wire Line
	4800 1800 5050 1800
Wire Wire Line
	4800 2100 5050 2100
Wire Wire Line
	4800 2400 5050 2400
$Comp
L power:GND #PWR016
U 1 1 609EAB24
P 5750 3100
F 0 "#PWR016" H 5750 2850 50  0001 C CNN
F 1 "GND" H 6100 3150 50  0000 C CNN
F 2 "" H 5750 3100 50  0001 C CNN
F 3 "" H 5750 3100 50  0001 C CNN
	1    5750 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3000 5750 3100
$Comp
L Device:C C5
U 1 1 609EDB53
P 6750 1650
F 0 "C5" H 6865 1696 50  0000 L CNN
F 1 "10 nF" H 6865 1605 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 6788 1500 50  0001 C CNN
F 3 "~" H 6750 1650 50  0001 C CNN
	1    6750 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 609EF3DE
P 6750 2150
F 0 "C6" H 6865 2196 50  0000 L CNN
F 1 "10 nF" H 6865 2105 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 6788 2000 50  0001 C CNN
F 3 "~" H 6750 2150 50  0001 C CNN
	1    6750 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 609EFE53
P 7050 2650
F 0 "R3" H 7120 2696 50  0000 L CNN
F 1 "2.2 k" H 7120 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 6980 2650 50  0001 C CNN
F 3 "~" H 7050 2650 50  0001 C CNN
	1    7050 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 1500 6750 1500
Wire Wire Line
	6450 1800 6750 1800
Wire Wire Line
	6450 2000 6750 2000
Wire Wire Line
	6450 2300 6750 2300
$Comp
L power:GND #PWR020
U 1 1 60A181EE
P 7050 2800
F 0 "#PWR020" H 7050 2550 50  0001 C CNN
F 1 "GND" H 7200 2700 50  0000 C CNN
F 2 "" H 7050 2800 50  0001 C CNN
F 3 "" H 7050 2800 50  0001 C CNN
	1    7050 2800
	1    0    0    -1  
$EndComp
Text Label 7750 2500 0    50   ~ 0
CSEN1
Connection ~ 7050 2500
$Comp
L Connector:Screw_Terminal_01x02 J6
U 1 1 60A1A264
P 7550 1850
F 0 "J6" H 7630 1842 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 7630 1751 50  0000 L CNN
F 2 "TerminalBlock_TE-Connectivity:TerminalBlock_TE_282834-2_1x02_P2.54mm_Horizontal" H 7550 1850 50  0001 C CNN
F 3 "~" H 7550 1850 50  0001 C CNN
	1    7550 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 60A1B678
P 5000 1200
F 0 "#PWR014" H 5000 950 50  0001 C CNN
F 1 "GND" V 4850 1100 50  0000 C CNN
F 2 "" H 5000 1200 50  0001 C CNN
F 3 "" H 5000 1200 50  0001 C CNN
	1    5000 1200
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J4
U 1 1 60A1F9D2
P 5850 650
F 0 "J4" V 5912 794 50  0000 L CNN
F 1 "Conn_01x03_Male" V 6003 794 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 5850 650 50  0001 C CNN
F 3 "~" H 5850 650 50  0001 C CNN
	1    5850 650 
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 1200 5750 1200
Wire Wire Line
	5000 1200 5250 1200
Wire Wire Line
	5750 850  5750 1200
Connection ~ 5750 1200
$Comp
L power:+12V #PWR018
U 1 1 60A2347C
P 5850 950
F 0 "#PWR018" H 5850 800 50  0001 C CNN
F 1 "+12V" H 5800 1150 50  0000 C CNN
F 2 "" H 5850 950 50  0001 C CNN
F 3 "" H 5850 950 50  0001 C CNN
	1    5850 950 
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 950  5850 850 
Wire Wire Line
	5950 850  5950 1000
Wire Wire Line
	5950 1000 6100 1000
Connection ~ 6750 1500
Connection ~ 6750 2300
Wire Wire Line
	6750 1500 7350 1500
Wire Wire Line
	7350 1500 7350 1850
Wire Wire Line
	6750 2300 7150 2300
Wire Wire Line
	7350 1950 7150 1950
Wire Wire Line
	7150 1950 7150 2300
$Comp
L Connector:Screw_Terminal_01x02 J8
U 1 1 60A3DE6E
P 9300 1350
F 0 "J8" H 9380 1342 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 9380 1251 50  0000 L CNN
F 2 "TerminalBlock_TE-Connectivity:TerminalBlock_TE_282834-2_1x02_P2.54mm_Horizontal" H 9300 1350 50  0001 C CNN
F 3 "~" H 9300 1350 50  0001 C CNN
	1    9300 1350
	1    0    0    -1  
$EndComp
Text Label 8900 1350 2    50   ~ 0
CUST1
$Comp
L power:GND #PWR024
U 1 1 60A41F79
P 8900 1550
F 0 "#PWR024" H 8900 1300 50  0001 C CNN
F 1 "GND" H 9050 1500 50  0000 C CNN
F 2 "" H 8900 1550 50  0001 C CNN
F 3 "" H 8900 1550 50  0001 C CNN
	1    8900 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 1350 8900 1350
Wire Wire Line
	9100 1450 8900 1450
Wire Wire Line
	8900 1450 8900 1550
$Comp
L OCI_UPL_2_Capacitors:10uF_t C3
U 1 1 60A4F2BC
P 5400 1200
F 0 "C3" V 5250 1150 50  0000 L CNN
F 1 "10uF_t" V 5150 1050 50  0000 L CNN
F 2 "OCI_UPL_FOOTPRINTS:C_2312" H 5400 950 30  0001 C CNN
F 3 "https://www.digikey.com/short/qcd8n7" H 5400 1400 30  0001 C CNN
F 4 "T491C106K025AT" H 5400 1450 30  0001 C CNN "Part #"
F 5 "2.021" H 5400 1000 30  0001 C CNN "UPL #"
	1    5400 1200
	0    1    1    0   
$EndComp
Text Label 6100 1000 0    50   ~ 0
CUST1
Text Label 2350 4100 0    50   ~ 0
CSEN1
Text Label 2350 4200 0    50   ~ 0
CSEN2
Wire Wire Line
	2250 4100 2350 4100
Wire Wire Line
	2250 4200 2350 4200
Wire Wire Line
	6450 2500 7050 2500
Text Label 6600 2750 0    50   ~ 0
THRM1
Wire Wire Line
	6450 2600 6500 2600
Wire Wire Line
	6500 2600 6500 2750
Wire Wire Line
	6500 2750 6600 2750
$Comp
L Driver_Motor:LMD18200 U2
U 1 1 60AE3C2A
P 5750 5000
F 0 "U2" H 4900 5700 50  0000 C CNN
F 1 "LMD18200" H 4900 5600 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-11_P3.4x5.08mm_StaggerOdd_Lead4.85mm_Vertical" H 4300 3650 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmd18200.pdf" H 5650 5000 50  0001 C CNN
	1    5750 5000
	1    0    0    -1  
$EndComp
Text Label 4800 4700 2    50   ~ 0
MC2
Text Label 4800 5000 2    50   ~ 0
DIR2
Text Label 4800 5300 2    50   ~ 0
BR2
Wire Wire Line
	4800 4700 5050 4700
Wire Wire Line
	4800 5000 5050 5000
Wire Wire Line
	4800 5300 5050 5300
$Comp
L power:GND #PWR017
U 1 1 60AE3C36
P 5750 6000
F 0 "#PWR017" H 5750 5750 50  0001 C CNN
F 1 "GND" H 6100 6050 50  0000 C CNN
F 2 "" H 5750 6000 50  0001 C CNN
F 3 "" H 5750 6000 50  0001 C CNN
	1    5750 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 5900 5750 6000
$Comp
L Device:C C7
U 1 1 60AE3C3D
P 6750 4550
F 0 "C7" H 6865 4596 50  0000 L CNN
F 1 "10 nF" H 6865 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 6788 4400 50  0001 C CNN
F 3 "~" H 6750 4550 50  0001 C CNN
	1    6750 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 60AE3C43
P 6750 5050
F 0 "C8" H 6865 5096 50  0000 L CNN
F 1 "10 nF" H 6865 5005 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 6788 4900 50  0001 C CNN
F 3 "~" H 6750 5050 50  0001 C CNN
	1    6750 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 60AE3C4F
P 7050 5550
F 0 "R4" H 7120 5596 50  0000 L CNN
F 1 "2.2 k" H 7120 5505 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 6980 5550 50  0001 C CNN
F 3 "~" H 7050 5550 50  0001 C CNN
	1    7050 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 4400 6750 4400
Wire Wire Line
	6450 4700 6750 4700
Wire Wire Line
	6450 4900 6750 4900
Wire Wire Line
	6450 5200 6750 5200
$Comp
L power:GND #PWR021
U 1 1 60AE3C59
P 7050 5700
F 0 "#PWR021" H 7050 5450 50  0001 C CNN
F 1 "GND" H 7200 5600 50  0000 C CNN
F 2 "" H 7050 5700 50  0001 C CNN
F 3 "" H 7050 5700 50  0001 C CNN
	1    7050 5700
	1    0    0    -1  
$EndComp
Text Label 7750 5400 0    50   ~ 0
CSEN2
Connection ~ 7050 5400
$Comp
L Connector:Screw_Terminal_01x02 J7
U 1 1 60AE3C62
P 7550 4750
F 0 "J7" H 7630 4742 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 7630 4651 50  0000 L CNN
F 2 "TerminalBlock_TE-Connectivity:TerminalBlock_TE_282834-2_1x02_P2.54mm_Horizontal" H 7550 4750 50  0001 C CNN
F 3 "~" H 7550 4750 50  0001 C CNN
	1    7550 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 60AE3C68
P 5000 4100
F 0 "#PWR015" H 5000 3850 50  0001 C CNN
F 1 "GND" V 4850 4000 50  0000 C CNN
F 2 "" H 5000 4100 50  0001 C CNN
F 3 "" H 5000 4100 50  0001 C CNN
	1    5000 4100
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J5
U 1 1 60AE3C74
P 5850 3550
F 0 "J5" V 5912 3694 50  0000 L CNN
F 1 "Conn_01x03_Male" V 6003 3694 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 5850 3550 50  0001 C CNN
F 3 "~" H 5850 3550 50  0001 C CNN
	1    5850 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 4100 5750 4100
Wire Wire Line
	5000 4100 5250 4100
Wire Wire Line
	5750 3750 5750 4100
Connection ~ 5750 4100
$Comp
L power:+12V #PWR019
U 1 1 60AE3C7F
P 5850 3850
F 0 "#PWR019" H 5850 3700 50  0001 C CNN
F 1 "+12V" H 5800 4050 50  0000 C CNN
F 2 "" H 5850 3850 50  0001 C CNN
F 3 "" H 5850 3850 50  0001 C CNN
	1    5850 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 3850 5850 3750
Wire Wire Line
	5950 3750 5950 3900
Wire Wire Line
	5950 3900 6100 3900
Connection ~ 6750 4400
Connection ~ 6750 5200
Wire Wire Line
	6750 4400 7350 4400
Wire Wire Line
	7350 4400 7350 4750
Wire Wire Line
	6750 5200 7150 5200
Wire Wire Line
	7350 4850 7150 4850
Wire Wire Line
	7150 4850 7150 5200
$Comp
L Connector:Screw_Terminal_01x02 J9
U 1 1 60AE3C98
P 9300 4250
F 0 "J9" H 9380 4242 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 9380 4151 50  0000 L CNN
F 2 "TerminalBlock_TE-Connectivity:TerminalBlock_TE_282834-2_1x02_P2.54mm_Horizontal" H 9300 4250 50  0001 C CNN
F 3 "~" H 9300 4250 50  0001 C CNN
	1    9300 4250
	1    0    0    -1  
$EndComp
Text Label 8900 4250 2    50   ~ 0
CUST2
$Comp
L power:GND #PWR025
U 1 1 60AE3C9F
P 8900 4450
F 0 "#PWR025" H 8900 4200 50  0001 C CNN
F 1 "GND" H 9050 4400 50  0000 C CNN
F 2 "" H 8900 4450 50  0001 C CNN
F 3 "" H 8900 4450 50  0001 C CNN
	1    8900 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 4250 8900 4250
Wire Wire Line
	9100 4350 8900 4350
Wire Wire Line
	8900 4350 8900 4450
$Comp
L OCI_UPL_2_Capacitors:10uF_t C4
U 1 1 60AE3CB0
P 5400 4100
F 0 "C4" V 5250 4050 50  0000 L CNN
F 1 "10uF_t" V 5150 3950 50  0000 L CNN
F 2 "OCI_UPL_FOOTPRINTS:C_2312" H 5400 3850 30  0001 C CNN
F 3 "https://www.digikey.com/short/qcd8n7" H 5400 4300 30  0001 C CNN
F 4 "T491C106K025AT" H 5400 4350 30  0001 C CNN "Part #"
F 5 "2.021" H 5400 3900 30  0001 C CNN "UPL #"
	1    5400 4100
	0    1    1    0   
$EndComp
Text Label 6100 3900 0    50   ~ 0
CUST2
Wire Wire Line
	6450 5400 7050 5400
Text Label 6600 5650 0    50   ~ 0
THRM2
Wire Wire Line
	6450 5500 6500 5500
Wire Wire Line
	6500 5500 6500 5650
Wire Wire Line
	6500 5650 6600 5650
Text Label 1200 3900 2    50   ~ 0
MC1
Text Label 1200 3500 2    50   ~ 0
MC2
Text Label 1200 4000 2    50   ~ 0
BR1
Text Label 1200 3600 2    50   ~ 0
BR2
Text Label 1200 3400 2    50   ~ 0
DIR2
Text Label 1200 4100 2    50   ~ 0
THRM1
Text Label 1200 3700 2    50   ~ 0
THRM2
Wire Wire Line
	1200 3400 1250 3400
Wire Wire Line
	1200 3500 1250 3500
Wire Wire Line
	1200 3600 1250 3600
Wire Wire Line
	1200 3700 1250 3700
Wire Wire Line
	1200 3800 1250 3800
Wire Wire Line
	1200 3900 1250 3900
Wire Wire Line
	1200 4000 1250 4000
Wire Wire Line
	1200 4100 1250 4100
$Comp
L power:+5V #PWR010
U 1 1 60B192F8
P 1600 6600
F 0 "#PWR010" H 1600 6450 50  0001 C CNN
F 1 "+5V" H 1615 6773 50  0000 C CNN
F 2 "" H 1600 6600 50  0001 C CNN
F 3 "" H 1600 6600 50  0001 C CNN
	1    1600 6600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR011
U 1 1 60B1B89F
P 1600 6750
F 0 "#PWR011" H 1600 6500 50  0001 C CNN
F 1 "GND" H 1600 6550 50  0000 C CNN
F 2 "" H 1600 6750 50  0001 C CNN
F 3 "" H 1600 6750 50  0001 C CNN
	1    1600 6750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x04 J2
U 1 1 60B1BD7F
P 1950 6500
F 0 "J2" H 2030 6492 50  0000 L CNN
F 1 "Screw_Terminal_01x04" H 2030 6401 50  0000 L CNN
F 2 "TerminalBlock_TE-Connectivity:TerminalBlock_TE_282834-4_1x04_P2.54mm_Horizontal" H 1950 6500 50  0001 C CNN
F 3 "~" H 1950 6500 50  0001 C CNN
	1    1950 6500
	1    0    0    -1  
$EndComp
Text Label 1600 6400 2    50   ~ 0
A1
Text Label 1600 6500 2    50   ~ 0
B1
Wire Wire Line
	1600 6400 1750 6400
Wire Wire Line
	1600 6500 1750 6500
Wire Wire Line
	1600 6600 1750 6600
Wire Wire Line
	1600 6750 1600 6700
Wire Wire Line
	1600 6700 1750 6700
$Comp
L power:+5V #PWR012
U 1 1 60B2EF7D
P 3450 6600
F 0 "#PWR012" H 3450 6450 50  0001 C CNN
F 1 "+5V" H 3465 6773 50  0000 C CNN
F 2 "" H 3450 6600 50  0001 C CNN
F 3 "" H 3450 6600 50  0001 C CNN
	1    3450 6600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 60B2EF83
P 3450 6750
F 0 "#PWR013" H 3450 6500 50  0001 C CNN
F 1 "GND" H 3450 6550 50  0000 C CNN
F 2 "" H 3450 6750 50  0001 C CNN
F 3 "" H 3450 6750 50  0001 C CNN
	1    3450 6750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x04 J3
U 1 1 60B2EF89
P 3800 6500
F 0 "J3" H 3880 6492 50  0000 L CNN
F 1 "Screw_Terminal_01x04" H 3880 6401 50  0000 L CNN
F 2 "TerminalBlock_TE-Connectivity:TerminalBlock_TE_282834-4_1x04_P2.54mm_Horizontal" H 3800 6500 50  0001 C CNN
F 3 "~" H 3800 6500 50  0001 C CNN
	1    3800 6500
	1    0    0    -1  
$EndComp
Text Label 3450 6400 2    50   ~ 0
A2
Text Label 3450 6500 2    50   ~ 0
B2
Wire Wire Line
	3450 6600 3600 6600
Wire Wire Line
	3450 6750 3450 6700
Wire Wire Line
	3450 6700 3600 6700
Text Label 2400 3500 0    50   ~ 0
A1
Text Label 2400 3600 0    50   ~ 0
B1
Wire Wire Line
	2400 3600 2250 3600
Wire Wire Line
	2400 3500 2250 3500
Wire Wire Line
	3450 6500 3600 6500
Wire Wire Line
	3450 6400 3600 6400
Text Label 2400 3700 0    50   ~ 0
A2
Text Label 2400 3800 0    50   ~ 0
B2
Wire Wire Line
	2400 3700 2250 3700
Wire Wire Line
	2400 3800 2250 3800
NoConn ~ 1250 4200
Text Label 1200 3800 2    50   ~ 0
DIR1
Wire Wire Line
	5750 1200 5750 1300
$Comp
L Device:C C10
U 1 1 60A0736C
P 7500 2650
F 0 "C10" H 7615 2696 50  0000 L CNN
F 1 "10 nF" H 7615 2605 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 7538 2500 50  0001 C CNN
F 3 "~" H 7500 2650 50  0001 C CNN
	1    7500 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR023
U 1 1 60A079EA
P 7500 2800
F 0 "#PWR023" H 7500 2550 50  0001 C CNN
F 1 "GND" H 7650 2700 50  0000 C CNN
F 2 "" H 7500 2800 50  0001 C CNN
F 3 "" H 7500 2800 50  0001 C CNN
	1    7500 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 2500 7500 2500
Connection ~ 7500 2500
Wire Wire Line
	7500 2500 7750 2500
Wire Wire Line
	5750 4100 5750 4200
$Comp
L Device:C C9
U 1 1 60A0F954
P 7450 5550
F 0 "C9" H 7565 5596 50  0000 L CNN
F 1 "10 nF" H 7565 5505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 7488 5400 50  0001 C CNN
F 3 "~" H 7450 5550 50  0001 C CNN
	1    7450 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 60A10703
P 7450 5700
F 0 "#PWR022" H 7450 5450 50  0001 C CNN
F 1 "GND" H 7600 5600 50  0000 C CNN
F 2 "" H 7450 5700 50  0001 C CNN
F 3 "" H 7450 5700 50  0001 C CNN
	1    7450 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 5400 7450 5400
Connection ~ 7450 5400
Wire Wire Line
	7450 5400 7750 5400
$Comp
L MCU_Module:Arduino_Nano_v3.x A1
U 1 1 5FCAD89B
P 1750 3500
F 0 "A1" H 1750 2411 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 2250 2550 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 1750 3500 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 1750 3500 50  0001 C CNN
	1    1750 3500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
